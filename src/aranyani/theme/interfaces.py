from zope.interface import Interface
from zope import schema
from plone.theme.interfaces import IDefaultPloneLayer
from aranyani.theme import _


class IaranyaniThemeSettings(Interface):

    contactName = schema.TextLine(title=u"Contact name", required=False)
    contactAddress = schema.TextLine(title=u"Contact address", required=False)
    contactEmail = schema.TextLine(title=u"Contact emailaddress", required=False)
    contactPhone = schema.TextLine(title=u"Contact phonenumber", required=False)
    contactTwitter = schema.TextLine(title=u"Twitter url", required=False)
    contactFacebook = schema.TextLine(title=u"Facebook url", required=False)
    contactGplus = schema.TextLine(title=u"Google+ url", required=False)
    contactLinkedIn = schema.TextLine(title=u"LinkedIn url", required=False)
    themeCss = schema.TextLine(title=u"Theme CSS:",
                                 description=_(u"define a css file to override theme"),
                                 required=False)
    
    
class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope 3 skin layer bound to a Skin
       Selection in portal_skins.
       If you need to register a viewlet only for the "YourSkin"
       skin, this is the interface that must be used for the layer attribute
       in YourSkin/browser/configure.zcml.
    """