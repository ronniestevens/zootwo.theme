# Zope imports
from plone.app.layout.viewlets import ViewletBase

# access the contact settings
from zope.component import getUtility
from plone.registry.interfaces import IRegistry
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone import api


class ContactViewlet(ViewletBase):
    """A viewlet showing some site_properties
    """

    index = ViewPageTemplateFile('templates/contactviewlet.pt')

    @property
    def available(self):
        return bool(self.getcontact())

    def getcontact(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            data = {
                'name': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactName'
                ],
                'address': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactAddress'
                ],
                'phone': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactPhone'
                ],
                'email': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactEmail'
                ],
            }

            return data


class SocialViewlet(ViewletBase):
    """A viewlet showing some site_properties
    """

    index = ViewPageTemplateFile('templates/socialviewlet.pt')

    @property
    def available(self):
        return bool(self.getsocial())

    def getsocial(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            socialdata = {
                'twitter': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactTwitter'],
                'facebook': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactFacebook'],
                'googleplus': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactGplus'],
                'linkedin': registry[
                    'aranyani.theme.interfaces.'
                    + 'IaranyaniThemeSettings.contactLinkedIn'],
            }
            return socialdata


class ColorViewlet(ViewletBase):
    """ a dummy viewlet containing the theme color"""

    index = ViewPageTemplateFile('templates/colorviewlet.pt')

    @property
    def available(self):
        return bool(self.getcolor())

    def getcolor(self):
        registry = getUtility(IRegistry)
        if registry != 0:
            color = registry[
                'aranyani.theme.interfaces.'
                + 'IaranyaniThemeSettings.themeCss']
        return color


class DropdownViewlet(ViewletBase):
    """A dropdown menu with only one level
    """
    index = ViewPageTemplateFile('templates/dropdownviewlet.pt')

    def available(self):
        if self.dropdownmenu():
            return True
        else:
            return False

    def dropdownmenu(self):
        catalog = api.portal.get_tool('portal_catalog')
        result = []
        context = self.context
        nav_root = api.portal.get_navigation_root(context=context)
        path = '/'.join(nav_root.getPhysicalPath())
        for parent in catalog(
            {'portal_type': (
                'Folder', 'Document', 'Event', 'News Item', ),
                'sort_on': 'getObjPositionInParent',
                'path': dict(query=path, depth=1)}):
            if parent.exclude_from_nav:
                continue
            parentpath = parent.getPath()
            children = catalog(
                {'portal_type': ('Document', 'Event', 'Folder', 'News Item', ),
                 'path': dict(query=parentpath, depth=1),
                 'sort_on': 'getObjPositionInParent'})
            result.append(
                dict(parent=parent,
                     children=children)
            )
        return result


class AccountViewlet(ViewletBase):
    """replace default login
    """

    index = ViewPageTemplateFile('templates/accountviewlet.pt')
    pass
